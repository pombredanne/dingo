=====
dingo
=====

*dingo* is a `Django`_ application which extends the `Django Admin`_
to allow the easy creation of additional views for Model classes and
instances.  dingo also allows you to customize the Admin of third
party applications, without subclassing or modifying the original
source tree.

Features
========

* Easily create new views for your Model Admin, either at the Model or
  object level.
* Inject customizations into the admin declaration of a third
  party application without modifying the original code base.

Usage
=====

To use dingo, ensure that it's on the Python path, and add it to your
``INSTALLED_APPS`` setting in ``settings.py``.  Note that dingo *must*
be installed *before* ``django.contrib.admin``, as it replaces the
default ``AdminSite`` with one which will instrument the
``ModelAdmin`` classes registered.

dingo requires Django 1.2 or later.

Models vs. Objects
------------------

dingo can be used to register views for Models or Objects.  Views
registered for Models are not specific to any instance of the model;
they can be thought of as similar to Django admin actions with an
[potentially] empty queryset.  Instance views operate on single model
instances; you can think of them as similar to admin actions that
operate on a single object instead of a queryset.

dingo views differ from `admin actions`_ in a couple of subtle ways.
The most important difference is that they may be injected into third
party applications wihthout sub-classing or modifying the application
code.  dingo views also have a different user interface by default.
dingo includes replacement admin templates which show dingo views as
buttons in the upper right hand area of the change list and change
form views.

Adding Views
------------

If you want to use dingo to add model or object views to your Model's
admin, you can define them either as functions or as methods on the
ModelAdmin.

For example, to define the view as a function::

  import dingo
  import dingo_test.models

  @dingo.object_view(dingo_test.models.RstDocument)
  @dingo.short_description("Render")
  def render(model_admin, request, object_id):

      from django.shortcuts import redirect
      document = dingo_test.mdoels.RstDocument.objects.get(id=object_id)

      return Response()

The same view may also be a method on the ModelAdmin::

  class RstDocumentAdmin(ModelAdmin):

      @dingo.object_view(dingo_test.models.RstDocument)
      @dingo.short_description("Render")
      def render(self, request, object_id):
          pass

Note that the use of the ``short_description`` decorator defines the
label used in the admin user interface, and is optional in this case.
If no description is provided, dingo will format the function or
method name, similar to how Django formats default verbose names for
models.

Adding Views to Third Party Applications
----------------------------------------

So long as dingo is listed in ``INSTALLED_APPS`` before
``django.contrib.admin``, the function-based example above will work
as desired.  Note that the code needs to be imported to ensure that
registration occurs properly.

Missing Features / Bugs
=======================

* Model views are significantly less developed than Instance views.
* Different actions for Models in different AdminSites are not
  currently supported.  To accomodate this, a site-local action
  registry will need to be used.

.. _django: http://djangoproject.com/
.. _Django Admin: http://docs.djangoproject.com/en/1.1/ref/contrib/admin/
.. _admin actions: http://docs.djangoproject.com/en/1.1/ref/contrib/admin/actions/#ref-contrib-admin-actions
